<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-09-04 14:50:35 by joricklochet-->
<display version="2.0.0">
  <name>CV_ControlValve_Horizontal_BlockIcon_compact</name>
  <width>158</width>
  <height>180</height>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP</name>
    <pv_name>$(WIDESSDeviceName):ValveSetpoint</pv_name>
    <y>144</y>
    <width>156</width>
    <height>34</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_Positin</name>
    <pv_name>$(WIDESSDeviceName):ValvePosition</pv_name>
    <y>114</y>
    <width>156</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Actual position</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon</name>
    <pv_name>$(WIDESSDeviceName):DeviceColor</pv_name>
    <symbols>
      <symbol>../symbols/valve_analog_CLOSED@64.png</symbol>
      <symbol>../symbols/valve_analog_OK@64.png</symbol>
      <symbol>../symbols/valve_analog_ERROR@64.png</symbol>
      <symbol>../symbols/valve_analog_OFF@64.png</symbol>
      <symbol>../symbols/valve_analog_NEUTRAL@64.png</symbol>
    </symbols>
    <x>46</x>
    <y>50</y>
    <width>64</width>
    <height>64</height>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>A</text>
    <x>10</x>
    <y>27</y>
    <width>27</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="24.0">
      </font>
    </font>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value> </value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Manual</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>$(WIDDeviceName)</text>
    <x>8</x>
    <y>1</y>
    <width>140</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="28.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../../CommonSymbols/error@32.png</symbol>
    </symbols>
    <x>123</x>
    <y>34</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Moving</name>
    <symbols>
      <symbol>../../../CommonSymbols/opening_neutral@32.png</symbol>
      <symbol>../../../CommonSymbols/closing_neutral@32.png</symbol>
    </symbols>
    <x>103</x>
    <y>52</y>
    <width>23</width>
    <height>24</height>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="(pv0 == 0) &amp;&amp; (pv1 == 0)">
          <value>false</value>
        </exp>
        <exp bool_exp="(pv0 == 1) || (pv1 == 1)">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):Opening</pv_name>
        <pv_name>$(WIDESSDeviceName):Closing</pv_name>
      </rule>
      <rule name="Picture" prop_id="initial_index" out_exp="false">
        <exp bool_exp="(pv0 == 1) &amp;&amp; (pv1 == 0)">
          <value>0</value>
        </exp>
        <exp bool_exp="(pv0 == 0) &amp;&amp; (pv1 == 1)">
          <value>1</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):Opening</pv_name>
        <pv_name>$(WIDESSDeviceName):Closing</pv_name>
      </rule>
    </rules>
    <tooltip>Pump is ramping</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../../CommonSymbols/interlock_overridden_disabled_cms@32.png</symbol>
    </symbols>
    <x>3</x>
    <y>59</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupInterlock</pv_name>
      </rule>
      <rule name="Tooltip" prop_id="tooltip" out_exp="true">
        <exp bool_exp="true">
          <expression>pvStr0</expression>
        </exp>
        <pv_name>$(WIDESSDeviceName):InterlockMsg</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock Message</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/ControlValve_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>$(WIDESSDeviceName)</ESSDeviceName>
          <DeviceName>$(WIDDeviceName)</DeviceName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>158</width>
    <height>114</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>SP:</text>
    <y>150</y>
    <width>30</width>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="20.0">
      </font>
    </font>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>PV:</text>
    <y>119</y>
    <width>30</width>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="20.0">
      </font>
    </font>
  </widget>
</display>
