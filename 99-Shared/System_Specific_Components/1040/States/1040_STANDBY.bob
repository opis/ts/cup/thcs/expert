<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-07-24 15:23:32 by EpingerBalint-->
<display version="2.0.0">
  <name>1040 - STANDBY</name>
  <macros>
    <PLCName>Tgt-RGT1040:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <Title>Radiolysis Gas Treatment System - 1040 - Standby State</Title>
    </macros>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X2_1</name>
    <macros>
      <Faceplate>../../System_Specific_Components/1040/States/1040_STANDBY.bob</Faceplate>
      <StepName>STANDBY</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>002</WIDIndex>
      <WIDSecSub>Tgt-RGT1040</WIDSecSub>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>210</y>
    <width>500</width>
    <height>350</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_2</name>
    <text>State status</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>1040_RUNNING.bob</file>
        <macros>
          <StepDeviceName>Tgt-RGT1040:SC-FSM-004</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To RUNNING</text>
    <x>2370</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open RUNNING state view</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <file>1040_OFF.bob</file>
        <macros>
          <StateDeviceName>Tgt-RGT1040:SC-FSM-001</StateDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To OFF</text>
    <x>30</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open OFF state view</confirm_message>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1_6</name>
    <macros>
      <Faceplate>../../System_Specific_Components/1040/States/1040_STANDBY.bob</Faceplate>
      <StateName>INERTING</StateName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>021</WIDIndex>
      <WIDSecSub>Tgt-RGT1040</WIDSecSub>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_SubState_Compact.bob</file>
    <x>2055</x>
    <y>730</y>
    <width>471</width>
    <height>220</height>
    <resize>1</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_3</name>
    <pv_name>${StepDeviceName}:MeasPerm1</pv_name>
    <x>950</x>
    <y>210</y>
    <width>180</width>
    <height>470</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <labels>
      <text>FI - 105 OK</text>
      <text>FI - 305 OK</text>
      <text>FI - 403 OK</text>
      <text>FI - 701 OK</text>
      <text>TI - 111 OK</text>
      <text>TI - 112 OK</text>
      <text>TI - 116 OK</text>
      <text>TI - 117 OK</text>
      <text>TI - 118 OK</text>
      <text>TI - 121 OK</text>
      <text>TI - 122 OK</text>
      <text>TI - 167 OK</text>
      <text>TI - 212 OK</text>
      <text>PI - 108 OK</text>
      <text>PI - 137 OK</text>
      <text>LI - 131 OK</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_4</name>
    <pv_name>${StepDeviceName}:MeasPerm2</pv_name>
    <x>1150</x>
    <y>210</y>
    <width>180</width>
    <height>411</height>
    <numBits>14</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <labels>
      <text>LS - 113 OK</text>
      <text>FS - 310 OK</text>
      <text>PS - 311 OK</text>
      <text>PS - 413 OK</text>
      <text>AI - 191 OK</text>
      <text>AI - 192 OK</text>
      <text>AI - 193 OK</text>
      <text>AI - 194 OK</text>
      <text>II - 195 OK</text>
      <text>JI - 196 OK</text>
      <text>Gas Analyzer OK</text>
      <text>Heaters OK</text>
      <text>Controlled Valves OK</text>
      <text>Pumps &amp; Blowers OK</text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1510</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>950</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>29</x>
    <y>680</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1_2</name>
    <macros>
      <Description>State Timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_2</name>
    <macros>
      <Description>Speed ratio between Blowers V-102:V-152</Description>
      <EngUnit>%</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>243</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3_2</name>
    <macros>
      <Description>FICA-105/155 Gas loop flow control SP</Description>
      <EngUnit>m3/h</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>276</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_1</name>
    <macros>
      <Description>LICA-131/151 Condensate tank level control  SP</Description>
      <EngUnit>%</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>309</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter5_2</name>
    <macros>
      <Description>TICA-116 Gas loop temperature control SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>344</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter6_2</name>
    <macros>
      <Description>PICA-137 Gas loop pressure control SP</Description>
      <EngUnit>bar(g)</EngUnit>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>377</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter7_3</name>
    <macros>
      <Description>AICA-193 Oxygen concentration control SP</Description>
      <EngUnit>g/min</EngUnit>
      <ParameterID>S7</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>29</x>
    <y>412</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_4</name>
    <text>Substates status</text>
    <x>2040</x>
    <y>680</y>
    <width>480</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_25</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1510</x>
    <y>210</y>
    <width>380</width>
    <height>250</height>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1040-AE-190 RAMAN</text>
      <text>1040-P-205</text>
      <text>1040-P-255</text>
      <text>1040-YSV-301</text>
      <text>1040-YSV-703</text>
      <text>1040-YSV-405</text>
      <text>1040-YSV-406</text>
      <text>1040-W-110</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
</display>
