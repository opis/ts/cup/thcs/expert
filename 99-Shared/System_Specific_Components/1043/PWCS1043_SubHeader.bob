<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-02 13:31:37 by EpingerBalint-->
<display version="2.0.0">
  <name>PWCM1041_SubHeader</name>
  <width>2550</width>
  <height>30</height>
  <background_color>
    <color name="Button_Background" red="236" green="236" blue="236">
    </color>
  </background_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>${Title}</text>
    <width>2550</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="20.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Menu6</name>
    <actions>
      <action type="open_display">
        <file>States/PWCS1043_FSMOverView.bob</file>
        <target>replace</target>
        <description>OverView</description>
      </action>
      <action type="open_display">
        <file>States/PWCS1043_OFF.bob</file>
        <macros>
          <Faceplate>PWCM1041_StateFaceplace_X1_OFF.bob</Faceplate>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-001</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>OFF</description>
      </action>
      <action type="open_display">
        <file>States/PWCS1043_STANDBY.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-002</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>STANDBY</description>
      </action>
      <action type="open_display">
        <file>States/PWCS1043_RUNNING.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-004</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>RUNNING</description>
      </action>
      <action type="open_display">
        <file>States/PWCS1043_MAINTENANCE.bob</file>
        <macros>
          <Faceplate>PWCS1043_MAINTENANCE.bob</Faceplate>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-006</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>MAINTENANCE</description>
      </action>
      <action type="open_display">
        <file>States/PWCS1043_FAULT.bob</file>
        <macros>
          <Faceplate>PWCS1043_FAULT.bob</Faceplate>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-010</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>FAULT</description>
      </action>
    </actions>
    <text>State Machine</text>
    <x>1</x>
    <width>140</width>
    <font>
      <font family="Source Code Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <transparent>true</transparent>
    <tooltip>Change display</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Change display</confirm_message>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_1</name>
    <macros>
      <ActiveAlarmAct>Tgt-SPCS:Ctrl-PLC-001:ActiveAlarmAct_1043</ActiveAlarmAct>
    </macros>
    <file>../../99-Shared/Diagnostics/ActiveAlarmOverview.bob</file>
    <x>2420</x>
    <y>2</y>
    <width>130</width>
    <height>25</height>
    <resize>3</resize>
    <tooltip>Currently Active Alarms (Enabled and Active/Defined and Active)</tooltip>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_2</name>
    <macros>
      <UndefinedAlarm>Tgt-SPCS:Ctrl-PLC-001:UnDefinedAlarm_1043</UndefinedAlarm>
    </macros>
    <file>../../99-Shared/Diagnostics/UndefinedAlarmOverview.bob</file>
    <x>2282</x>
    <y>2</y>
    <width>130</width>
    <height>25</height>
    <resize>3</resize>
    <tooltip>Total Alarms for Current State (Enabled/Defined)</tooltip>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_3</name>
    <macros>
      <ForcedAct>Tgt-SPCS:Ctrl-PLC-001:ForcedAct_1043</ForcedAct>
    </macros>
    <file>../../99-Shared/Diagnostics/ForceOverview.bob</file>
    <x>2143</x>
    <y>2</y>
    <width>130</width>
    <height>25</height>
    <resize>3</resize>
    <tooltip>Total Alarms for Current State (Enabled/Defined)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Menu2</name>
    <actions>
      <action type="open_display">
        <file>PWCS1043_Main.bob</file>
        <target>replace</target>
        <description>Main loop</description>
      </action>
    </actions>
    <text>Main loop</text>
    <x>160</x>
    <width>140</width>
    <font>
      <font family="Source Code Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <transparent>true</transparent>
    <tooltip>Change display</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Change display</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Menu3</name>
    <actions>
      <action type="open_display">
        <file>PWCS1043_Consumers.bob</file>
        <target>replace</target>
        <description>Consumers</description>
      </action>
    </actions>
    <text>Consumers</text>
    <x>320</x>
    <width>140</width>
    <font>
      <font family="Source Code Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <transparent>true</transparent>
    <tooltip>Change display</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Change display</confirm_message>
  </widget>
</display>
