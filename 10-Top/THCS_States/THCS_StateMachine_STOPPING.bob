<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-02-11 12:58:19 by HorváthLóránt-->
<display version="2.0.0">
  <name>THCS - STOPPING</name>
  <macros>
    <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2560</width>
  <height>1250</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>InterlockStatus_Title_2</name>
    <text>Failure actions status</text>
    <x>1410</x>
    <y>170</y>
    <width>250</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>InterlockStatus_Loop1_2</name>
    <pv_name>$(ESSDeviceName):IntlckDevPerm1</pv_name>
    <x>1150</x>
    <y>212</y>
    <width>250</width>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="Attention" red="252" green="242" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1010-V-001a</text>
      <text>1010-V-001b</text>
      <text>1010-V-002</text>
      <text>1010-YSV-001</text>
      <text>1010-YSV-002</text>
      <text>1010-YSV-006</text>
      <text>1010-YSV-005a</text>
      <text>1010-YSV-005b</text>
      <text>1010-YSV-024</text>
      <text>1010-YSV-025</text>
      <text>1011-YSV-011</text>
      <text>1011-YSV-010</text>
      <text>1011-YSV-016</text>
      <text>1011-YSV-018</text>
      <text>1011-YSV-230</text>
      <text></text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>1010-FIC-001 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S1</ParameterID>
      <EngUnit>kg/s</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>1011-PIC-010 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S2</ParameterID>
      <EngUnit>kPa</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>240</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>1011-PIC-020 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S3</ParameterID>
      <EngUnit>kPa</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>270</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../../99-Shared/System_Specific_Components/Header/THCS_Header.bob</file>
    <width>2560</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>X5 - STOPPING</text>
    <y>90</y>
    <width>2557</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_3</name>
    <text>State status</text>
    <x>2021</x>
    <y>170</y>
    <width>500</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_3</name>
    <text>State parameters</text>
    <x>40</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_3</name>
    <pv_name>$(ESSDeviceName):Permissive1</pv_name>
    <x>850</x>
    <y>212</y>
    <width>250</width>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <labels>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text>Current state activated</text>
    </labels>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>$(ESSDeviceName):STS_Active</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_25</name>
    <text>State permissives</text>
    <x>850</x>
    <y>170</y>
    <width>250</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>$(ESSDeviceName):STS_Active</pv_name>
      </rule>
    </rules>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>THCS_StateMachine_STANDBY.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:SC-FSM-002</ESSDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To STANDBY</text>
    <x>2390</x>
    <y>127</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <file>THCS_StateMachine_RUNNING.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:SC-FSM-004</ESSDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To RUNNING</text>
    <x>10</x>
    <y>127</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>MainBackRectangle_1</name>
    <x>2021</x>
    <y>530</y>
    <width>500</width>
    <height>45</height>
    <line_color>
      <color name="BLUE-BORDER" red="47" green="135" blue="148">
      </color>
    </line_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button_1</name>
    <pv_name>${StateDeviceName}:P_AutoStart</pv_name>
    <label>Sequencer auto start after activation</label>
    <x>2031</x>
    <y>538</y>
    <width>490</width>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="20.0">
      </font>
    </font>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>InterlockStatus_Title_1</name>
    <text>Interlocks status</text>
    <x>1150</x>
    <y>170</y>
    <width>250</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1_1</name>
    <macros>
      <Description>1013-PIC-033 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S4</ParameterID>
      <EngUnit>kPa</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>300</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_1</name>
    <macros>
      <Description>1016-TIC-601 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S5</ParameterID>
      <EngUnit>degC</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>330</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3_1</name>
    <macros>
      <Description>1016-SICA-601 Setpoint</Description>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <ParameterID>S6</ParameterID>
      <EngUnit>barg</EngUnit>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>360</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>1497</x>
    <y>1045</y>
    <width>50</width>
    <height>30</height>
    <line_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </background_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_1</name>
    <x>1497</x>
    <y>1010</y>
    <width>50</width>
    <height>30</height>
    <line_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>= Interlock OFF</text>
    <x>1556</x>
    <y>1013</y>
    <width>105</width>
    <height>25</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>= Interlock ON</text>
    <x>1556</x>
    <y>1049</y>
    <width>99</width>
    <height>25</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X2_3</name>
    <macros>
      <Faceplate>../../../10-Top/THCS_States/THCS_StateMachine_STOPPING.bob</Faceplate>
      <PLCName>Tgt-THCS:Ctrl-PLC-001</PLCName>
      <WIDDeviceName>STOPPING</WIDDeviceName>
      <WIDESSDeviceName>Tgt-HeC1010:SC-FSM-005</WIDESSDeviceName>
    </macros>
    <file>../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Standard.bob</file>
    <x>2021</x>
    <y>210</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>InterlockStatus_Loop1_5</name>
    <pv_name>$(ESSDeviceName):IntlckDevPerm2</pv_name>
    <x>1410</x>
    <y>212</y>
    <width>250</width>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="Attention" red="252" green="242" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1010-V-002</text>
      <text>1010-YSV-006</text>
      <text>1011-YSV-023b</text>
      <text>1011-YSV-022b</text>
      <text>1015-YSV-101</text>
      <text>1015-YSV-405</text>
      <text>1016-SC-601a &amp; 601b</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/TurboCirculator/faceplates/P_TurboCirculator_PID_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-SC-001a</ESSDeviceName>
          <DeviceName>SC-001a</DeviceName>
          <SystemName>1010</SystemName>
          <ESSControllerName>Tgt-HeC1010:Proc-FIC-001</ESSControllerName>
          <ControllerName>FIC-001</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>212</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_3</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/TurboCirculator/faceplates/P_TurboCirculator_PID_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-SC-001b</ESSDeviceName>
          <DeviceName>SC-001b</DeviceName>
          <SystemName>1010</SystemName>
          <ESSControllerName>Tgt-HeC1010:Proc-FIC-001</ESSControllerName>
          <ControllerName>FIC-001</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>260</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_4</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/ScrewCirculator/faceplates/P_ScrewCirculator_PID_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-SC-002</ESSDeviceName>
          <DeviceName>SC-002</DeviceName>
          <SystemName>1010</SystemName>
          <ESSControllerName>Tgt-HeC1010:Proc-FIC-001</ESSControllerName>
          <ControllerName>FIC-001</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>309</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_5</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-001</ESSDeviceName>
          <DeviceName>YSV-001</DeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>357</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_6</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-002</ESSDeviceName>
          <DeviceName>YSV-002</DeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>406</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_7</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-006</ESSDeviceName>
          <DeviceName>YSV-006</DeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>454</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_8</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-005a</DeviceName>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-005a</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>503</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_10</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-005b</DeviceName>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-005b</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>552</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_11</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-024</DeviceName>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-024</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>601</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_12</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-025</DeviceName>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-025</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>650</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_13</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-011</DeviceName>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-011</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>700</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_14</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-010</DeviceName>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-010</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>747</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_15</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-016</DeviceName>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-016</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>796</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_16</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-018</DeviceName>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-018</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>845</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_17</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_Faceplate.bob</file>
        <macros>
          <DeviceName>YSV-230</DeviceName>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-230</ESSDeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1150</x>
    <y>894</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_18</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/ScrewCirculator/faceplates/P_ScrewCirculator_PID_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-SC-002</ESSDeviceName>
          <DeviceName>SC-002</DeviceName>
          <SystemName>1010</SystemName>
          <ESSControllerName>Tgt-HeC1010:Proc-FIC-001</ESSControllerName>
          <ControllerName>FIC-001</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>212</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_19</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HeC1010:Proc-YSV-006</ESSDeviceName>
          <DeviceName>YSV-006</DeviceName>
          <SystemName>1010</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>261</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_20</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-023b</ESSDeviceName>
          <DeviceName>YSV-023b</DeviceName>
          <SystemName>1011</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>310</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_27</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HePC1011:Proc-YSV-022b</ESSDeviceName>
          <DeviceName>YSV-022b</DeviceName>
          <SystemName>1011</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>357</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_28</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HePu1015:Proc-YSV-101</ESSDeviceName>
          <DeviceName>YSV-101</DeviceName>
          <SystemName>1015</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>407</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_29</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Valves/valve solenoid/faceplates/OnOffValve_AUMA_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>Tgt-HePu1015:Proc-YSV-405</ESSDeviceName>
          <DeviceName>YSV-405</DeviceName>
          <SystemName>1015</SystemName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>457</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_31</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Pumps/faceplates/KSBPump_PIDController_Faceplate.bob</file>
        <macros>
          <WIDESSDeviceName>Tgt-HeCo1016:Proc-SC-601b</WIDESSDeviceName>
          <WIDDeviceName>SC-601b</WIDDeviceName>
          <SystemName>1016</SystemName>
          <ESSControllerName>Tgt-HeCo1016:Proc-PDICA-601</ESSControllerName>
          <ControllerName>PDICA-601</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>504</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <rules>
      <rule name="Visible" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>Tgt-THCS:Ctrl-PLC-001:FB_SC_601b_Selected</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_32</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/Pumps/faceplates/KSBPump_PIDController_Faceplate.bob</file>
        <macros>
          <WIDESSDeviceName>Tgt-HeCo1016:Proc-SC-601a</WIDESSDeviceName>
          <WIDDeviceName>SC-601a</WIDDeviceName>
          <SystemName>1016</SystemName>
          <ESSControllerName>Tgt-HeCo1016:Proc-PDICA-601</ESSControllerName>
          <ControllerName>PDICA-601</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <x>1410</x>
    <y>503</y>
    <width>250</width>
    <height>50</height>
    <transparent>true</transparent>
    <rules>
      <rule name="Visible" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>Tgt-THCS:Ctrl-PLC-001:FB_SC_601a_Selected</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
